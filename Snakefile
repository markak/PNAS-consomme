# Master Snakefile for Consomme, pipeline to find and characterize non-human
# component of DNA/RNA sequences.

# Usage:
# snakemake --cluster "sbatch --job-name={params.name} --ntasks=1 --cpus-per-task={threads} \
#           --partition={params.partition} --mem={params.mem} -o slurm_output/%j-{params.name}" -p -j -k

# Load data in pandas dataframes
import os
import pandas as pd
from collections import defaultdict
shell.prefix("set +euo pipefail;")
#shell.suffix("; echo")

test = pd.read_table("metadata/test_samples.txt", index_col=0)

data = pd.concat([test])

patient_dict = defaultdict(list)
for s in data.index:
  patient_dict[data.ix[s, "Patient"]].append(s)

cohort_dict = defaultdict(list)
for s in data.index:
  cohort_dict[data.ix[s, "Cohort"]].append(s)

VERSION = "3.0"

include: "Snakefile.preprocess"
include: "Snakefile.subtract"
include: "Snakefile.unknown"
include: "Snakefile.patient"
include: "Snakefile.cohort"
include: "Snakefile.last"
include: "Snakefile.utils"

localrules: all

def get_samples(index):
  return [(data.ix[s, "Cohort"], data.ix[s, "Patient"], s) for s in index]

s_s = get_samples(test.index)

rule all:
  input: expand("{sample[0]}/{sample[1]}/{sample[2]}/{sample[2]}.{stage}_stats.txt", \
           sample=s_s, stage=["preprocess","subtract"]),
         expand("{sample[0]}/cohort_assembly/spades/not_human/{sample[0]}.{suffix}", \
           sample=s_s, suffix=["bad", "nucl_nr.tab"]),
         #expand("{sample[0]}/cohort_assembly/spades/not_human/last/{sample[2]}.{sample[1]}.{sample[0]}.tab", \
         #  sample=s_s),
         #expand("{sample[0]}/cohort_assembly/spades/not_human/last/{sample[1]}.{sample[0]}.tab", \
         #  sample=s_s),
         #expand("{sample[0]}/{sample[1]}/patient_assembly/spades/not_human/last/{sample[2]}.{sample[1]}.tab", \
         #  sample=s_s),
         #expand("{sample[0]}/cohort_assembly/spades/not_human/{sample[2]}.{r}.fastq", \
         #  sample=s_s, r=["1", "2", "U"]),
