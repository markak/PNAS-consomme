#!/usr/bin/env python2

import sys
import HTSeq
from collections import defaultdict

fasta = HTSeq.FastaReader(sys.argv[1]).get_sequence_lengths()
dust = defaultdict(int)
with open(sys.argv[2]) as f:
  for line in f: 
    parts = line.split()
    dust[parts[0][1:]] += float(parts[2]) - float(parts[1])

for s in fasta:
  if dust[s]/fasta[s] >= 0.75:
    print s
