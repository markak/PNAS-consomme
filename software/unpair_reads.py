#!/usr/bin/env python
# Usage: ./unpair_reads.py <in.fq> <out1.fq> <out2.fq> 

import sys
import HTSeq

fq1 = open(sys.argv[2], "w")
fq2 = open(sys.argv[3], "w")
for seq in HTSeq.FastqReader(sys.argv[1]):
  if seq.name.endswith("/1"): seq.write_to_fastq_file(fq1)
  if seq.name.endswith("/2"): seq.write_to_fastq_file(fq2)
fq1.close()
fq2.close()
