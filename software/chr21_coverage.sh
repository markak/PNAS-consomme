#!/bin/bash
# Usage: ./chr21_covearge.sh <file> <version> <sample> <stage> <stat>

VERSION=$2
sample=$3
STAGE=$4
STAT=$5

COUNT="$(/local10G/resources/samtools-1.1/samtools depth $1 -r 21 | awk '{sum+=$3;cnt++}END{print sum}')"
COUNT="$(echo ${COUNT} '/' 48129895 | bc -l)"
echo -e "${VERSION}\t${sample}\t${STAGE}\t${STAT}\t${COUNT}"
