#!/bin/bash
# Usage: ./fastq_stats.sh <file> <version> <sample> <stage> <stat>

VERSION=$2
sample=$3
STAGE=$4
STAT=$5

COUNT="$(cat $1 | echo $((`wc -l`/4)))"
echo -e "${VERSION}\t${sample}\t${STAGE}\t${STAT}\t${COUNT}"
