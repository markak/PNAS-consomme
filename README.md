# Consomme-PNAS

Snakefiles and other assorted scripts used in [doi:10.1073/pnas.1707009114](http://www.pnas.org/content/114/36/9623).

Author is Mark Kowarsky markak@{this university}.edu

This pipeline is designed for running on SLURM or other similar cluster environment, with local storage on compute nodes.

An example metadata file is in metadata/test_samples.txt. The basic requirements for samples are:
* unique name
* patient
* cohort
* read type (single-end, paired-end, hybrid (both))
* list of reads from the read ends

## Dependencies:
* python + pandas
* Snakemake (pipeline)
* FastQC (QC of sequences)
* Trimmomatic (removing adapters and low quality bases)
* FLASH (merging overlapping reads)
* bowtie2 (short read aligner)
* samtools (manipulating SAM files)
* picard-tools (QC on SAM files)
* fq2fa (converts fastq to fasta)
* BLAST (general purpose aligner)
* SPADes (assembler)
* PRODIGAL (gene prediction)
* LAST (contig-contig aligner)

## Databases required:
* UniVec Core database
* Human genome (recommend toplevel from ENSEMBL)
* NCBI nt
* NCBI nr